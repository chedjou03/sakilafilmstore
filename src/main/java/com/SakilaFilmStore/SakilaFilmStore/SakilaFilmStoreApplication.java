package com.SakilaFilmStore.SakilaFilmStore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SakilaFilmStoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(SakilaFilmStoreApplication.class, args);
	}

}
