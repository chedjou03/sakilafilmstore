package com.SakilaFilmStore.SakilaFilmStore.Entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "actor")
public class Actor {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "actor_id")
	private Integer id;
	
	@Column(name = "first_name")
	private String firstName;
	
	@Column(name = "last_name")
	private String lastName;
	
	@Column(name = "last_update")
	private Date lastUpdate;
	
	public Actor() {
		super();
	}
	
	public Actor(Integer actorId, String actorFirstName, String actorLastName, Date actorLastUpdate) {
		super();
		this.id = actorId;
		this.firstName = actorFirstName;
		this.lastName = actorLastName;
		this.lastUpdate = actorLastUpdate;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer actorId) {
		this.id = actorId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String actorFirstName) {
		this.firstName = actorFirstName;
	}
	public String getActorLastName() {
		return lastName;
	}
	public void setLastName(String actorLastName) {
		this.lastName = actorLastName;
	}
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date actorLastUpdate) {
		this.lastUpdate = actorLastUpdate;
	}
	

	@Override
	public String toString() {
		return "Actor [actorId=" + id + ", actorFirstName=" + firstName + ", actorLastName=" + lastName
				+ ", actorLastUpdate=" + lastUpdate + "]";
	}
	

}
