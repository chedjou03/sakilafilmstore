package com.SakilaFilmStore.SakilaFilmStore.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.SakilaFilmStore.SakilaFilmStore.Entity.Actor;
import com.SakilaFilmStore.SakilaFilmStore.InventoryDao.InventoryDao;

@Service
public class FilmStoreServiceManager implements FilmStoreService{

	@Autowired
	private InventoryDao inventoryDao;
	
	@Override
	public List<Actor> getActors() {
		return inventoryDao.getActors();
	}

}
