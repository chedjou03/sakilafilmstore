package com.SakilaFilmStore.SakilaFilmStore.Service;

import java.util.List;

import com.SakilaFilmStore.SakilaFilmStore.Entity.Actor;

public interface FilmStoreService {

	List<Actor> getActors();

}
