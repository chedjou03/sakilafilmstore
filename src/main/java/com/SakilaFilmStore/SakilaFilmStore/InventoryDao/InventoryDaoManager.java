package com.SakilaFilmStore.SakilaFilmStore.InventoryDao;

import java.util.List;

import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.SakilaFilmStore.SakilaFilmStore.Entity.Actor;

@Repository
public class InventoryDaoManager implements InventoryDao {
	
		private EntityManager entityManager;
			
		@Autowired
		public InventoryDaoManager(EntityManager theEntityManager)
		{
			entityManager = theEntityManager;
		}

		@Override
		public List<Actor> getActors() {
			
			Session currentSession = entityManager.unwrap(Session.class);
			Query<Actor> theQuery = currentSession.createQuery("from Actor order by firstName",Actor.class);
			List<Actor> actors = theQuery.getResultList();	
			return actors;
		}

}
