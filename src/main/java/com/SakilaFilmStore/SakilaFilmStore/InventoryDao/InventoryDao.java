package com.SakilaFilmStore.SakilaFilmStore.InventoryDao;

import java.util.List;

import com.SakilaFilmStore.SakilaFilmStore.Entity.Actor;

public interface InventoryDao {

	List<Actor> getActors();

}
