package com.SakilaFilmStore.SakilaFilmStore.RestController;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.SakilaFilmStore.SakilaFilmStore.Entity.Actor;
import com.SakilaFilmStore.SakilaFilmStore.Service.FilmStoreService;

@RestController
@RequestMapping("filmStoreApi")
public class FilmStoreRestController {
	
	@Autowired
	private FilmStoreService filmStoreService;
	
	@GetMapping("/actors")
	public List<Actor> getActors() {
		return filmStoreService.getActors();
	}
	

}
